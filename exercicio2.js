console.log("Exercício 2 ----------------------------")
let valor = 35
if(valor <= 25) {
    console.log("O número está entre 0 e 25")
}
else if(25 < valor && valor <= 50) {
    console.log("O número está entre 25 e 50")
}
else if(50 < valor && valor <= 75) {
    console.log("O número está entre 50 e 75")
}
else if(75 < valor && valor <= 100) {
    console.log("O número está entre 75 e 100")
}
else {
    console.log("Fora do intervalo")
}
console.log("\n")